import os
from PIL import Image

try:
    img = input("Введите название картинки\n")
    image = Image.open( img )

    size = (int(input("Введите ширину\n")), int(input("Введите высоту\n")))
    for i in size:
        if i<100 or i>700:
            break
        image.thumbnail((size[0], size[1]), Image.LANCZOS)

        image_name = img.split('.')[0]
        image_type = img.split('.')[1]

        new_imagename = '{}_thumb_{}x{}.{}'.format( image_name, size[0], size[1], image_type )
        image.save( new_imagename)
        weight = os.path.getsize( new_imagename)
    print("Размер файла {} - {}  байт".format (new_imagename, weight))

except FileNotFoundError:
    print("Файл {} не найден".format(img))

except ValueError:
    print("Не удалось преобразовать введенный текст в число")

except NameError:
    print("Параметры ширины и высоты не должены быть менее 100 и более 700 пикселей")
