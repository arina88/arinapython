def letters_range(c1,c2, step=1):
    lst =[]
    for x in range(ord(c1), ord(c2), step):
        c=chr(x)
        lst+=c
    return lst
