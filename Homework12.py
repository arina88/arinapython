# Первый способ:
def fib(n):
    if n == 1 or n == 0:
        return 1
    return fib( n-2 ) + fib( n-1 )

# Второй способ:
def fib(n):
    a = 1
    b = 1
    for x in range(n):
        a, b = b, a + b
    return a

