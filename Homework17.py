from PIL import Image
from PIL.ExifTags import TAGS


def image_info(img):
    try:
        exifData = {}
        with Image.open( img ) as image:
            data = image._getexif()
            for tag, value in data.items():
                decoded = TAGS.get( tag )
                exifData[decoded] = value

                coordinates = exifData["GPSInfo"]
                latitude = coordinates[2]
                longitude = coordinates[4]
                lat_coord = ( float( latitude[0][0] / latitude[0][1] )
                           + float( latitude[1][0] / latitude[1][1]) / 60.0
                           + float(latitude[2][0] / latitude[2][1]) / 3600.0 )
                lon_coord = (float( longitude[0][0] / longitude[0][1] )
                             + float(longitude[1][0] / longitude[1][1] ) / 60.0
                             + float( longitude[2][0] / longitude[2][1] ) / 3600.0 )

                return (("latitude -{}, longitude-{}".format(  lat_coord, lon_coord )))

    except KeyError:
        return ("This photo does not have GPSInfo")

    except FileNotFoundError:
        return ("No such file: {}".format( img ))

    except AttributeError:
        return (" NoneType object has no attribute items")

