a = (int(input( "Введите целое число:\n" )))
count = 0

def col(a):
    if a % 2 == 0:
        return a / 2
    if a % 2 == 1:
        return a * 3 + 1

while True:
    a = col(a)
    count += 1
    print(a)
    if a <= 0:
        print( "Введите целое положительное число" )
        break
    if a == 1:
        print( "Всего получаем %d шагов" % (count) )
        break
