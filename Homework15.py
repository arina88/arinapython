class Router():
    def __init__(self):
        self.IPAddresses = []
        self.IPRoutes = []

    def __ipadd__(self, ip):
        if ip not in self.IPAddresses:
            self.IPAddresses.append(ip)
            return( "IP -address added" )
        else:
            return( "IP-address exists in the list" )


    def __ipremove__(self, ip):
        if ip in self.IPAddresses:
            self.IPAddresses.remove(ip)
            return( "IP-address removed" )
        else:
            return( "IP-address not found in the list" )

    def __iplst__(self):
        return (self.IPAddresses)

    def __routeadd__(self, route):
        if route not in self.IPRoutes:
            self.IPRoutes.append(route)
            return( "Route added" )
        else:
            return( "Route exists in the list" )


    def __routeremove__(self, route):
        if route in self.IPRoutes:
            self.IPRoutes.remove(route)
            return( "Route removed" )
        else:
            return( "Route not found in the list" )

    def __routelst__(self):
        return (self.IPRoutes)

