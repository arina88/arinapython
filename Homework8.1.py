def rec(s):
    if (len(s) > 0):
        return (int(s[0]))*10**(len(s)-1)+rec(s[1:])
    return 0

while True:
    try:
        s = input( "Введите любое число:\n" )
        if s == "cancel":
            print( "Bye" )
            break
        val = rec(s)
        if val  % 2 == 0:
             print(val /2)
        if val  % 2 == 1:
            print(val * 3 + 1)
    except ValueError:
            print("Не удалось преобразовать введенный текст в число")
