a = input( "Введите любые неотрицательные целые числа, разделите их пробелами:\n" )
a_list = [int( x ) for x in a.split( " " )]
for i in range( 1, len(a_list)+2):
    if not i in a_list:
        print( "Пропущенное значение:", i )
        break
