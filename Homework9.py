#6-Задание
print(sum([i for i in range(1, 101)])**2-sum([i**2 for i in range(1, 101)]))


#9-Задание
for a in range(1,1001):
    for b in range(1,1001):
        c=1000-a-b
        if a*a+b*b==c*c:
            print(a,b,c)


#40-Задание
lst = []
i = 1
while len(lst) < 1000000:
    lst.append(str(i))
    i+=1
lst = ''.join(lst)
print(int(lst[0])*int(lst[9])*int(lst[99])*int(lst[999])*int(lst[9999])*int(lst[99999])*int(lst[999999]))


#48-Задание
print(str(sum([i**i for i in range(1, 1001)]))[-10:])

